#!/bin/sh

docker run  -d \
       --name nginx \
       -v `pwd`/html:/usr/share/nginx/html:ro \
       --label-file `pwd`/labels.meta \
       -p 8081:80 nginx:alpine

docker run -d \
       --name nginx-exporter \
       -p 9113:9113 \
       nginx/nginx-prometheus-exporter:0.8.0 \
       -nginx.scrape-uri http://172.17.0.8:8080/stub_status
